class UserSecure{
    constructor(name, email){
        const emailRegResult= (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
  
        if(email.length <3 || email.length >400 || !emailRegResult){     
            throw new Error();
        }
        const result = /^[a-zA-Z ]+$/.test(name);
        if(name.length <2 ||name.length>100 || !result){
            throw new Error();
        }
        this.role="user";
        this.id = auth_user_id();
        this.name = name;
        this.email = email;
        Object.freeze(this);
    }
}

// A psudo authenticator 
// returns authenticated user ID
function auth_user_id() {
    return 2;
}


module.exports = { UserSecure, auth_user_id };
