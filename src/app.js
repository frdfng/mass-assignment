'use strict';

// requirements
const express = require('express');
const { User, UserSecure } = require('./user');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

app.get('/', (req, res) => {
    res.send('Post user preferences: name and email');
});

app.post('/', (req, res) => {
    if (req.body.email === undefined || req.body.name === undefined) {
        res.status(400).send('Please provide both email and name');
        return;
    }
    if("role" in req.body){
        res.status(400).send('wrong');
        
    }
    if("id" in req.body){
        res.status(400).send('wrong');
        
    }
    var size = Object.keys(req.body).length;
    if(size >2){
        res.status(400).send('wrong');
    }
try{
    const user = Object.freeze(new UserSecure(req.body.name, req.body.email));

    //var user =  Object.freeze(Object.assign(User, req.body));
    console.log(user);
    res.send('Your preferences have been successfully saved');
}catch(err){
    res.status(400).send('wrong');
}
});

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = app;
